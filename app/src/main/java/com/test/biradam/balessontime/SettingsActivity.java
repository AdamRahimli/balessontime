package com.test.biradam.balessontime;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.test.biradam.balessontime.dao.TableDao;
import com.test.biradam.balessontime.model.Table;
import com.test.biradam.balessontime.model.TableName;

import java.util.ArrayList;
import java.util.List;

public class SettingsActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    Button applyBtn;
    Button newBtn;
    Spinner spinner;

    TableDao tableDao = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        tableDao = new TableDao(this.openOrCreateDatabase("BALessonTime", MODE_PRIVATE, null));

        List<Table> tableNameList = tableDao.getTableList();
        List<String> tableNameListStr = new ArrayList<>();
        tableNameListStr.add("");
        for(Table t:tableNameList){
            tableNameListStr.add(t.getName());
        }


        applyBtn = (Button) findViewById(R.id.applyBtn);
        newBtn = (Button) findViewById(R.id.newBtn);
        spinner = (Spinner) findViewById(R.id.deployDataList);

        try{

        }catch (Exception ex){
            ex.printStackTrace();
        }
        ArrayAdapter dataAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, tableNameListStr);
        spinner.setAdapter(dataAdapter);
        spinner.setSelection(dataAdapter.getPosition(TableName.getTableName().getTable_name()));
    }

    public void changeTable(View view){
        try {
            TableName.getTableName().setTable_name(spinner.getSelectedItem().toString());
            Toast.makeText(this, "Complete", Toast.LENGTH_LONG).show();

            SQLiteDatabase myDatabase = this.openOrCreateDatabase("BALessonTime", MODE_PRIVATE, null);
            myDatabase.execSQL("CREATE TABLE IF NOT EXISTS dic (id INTEGER PRIMARY KEY AUTOINCREMENT ,dic_key VARCHAR , dic_value VARCHAR, add_date DATETIME DEFAULT CURRENT_TIMESTAMP)");
            myDatabase.execSQL("UPDATE dic SET dic_value = '"+spinner.getSelectedItem().toString()+"' WHERE dic_key = 'table_name'");
        }catch (Exception ex){
            Toast.makeText(this, "Error", Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }

    public void newTable(View view){
        Intent intent = new Intent(getApplicationContext(),AddTableActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.schedule) {
            Intent intent = new Intent(getApplicationContext(), ScheduleV3Activity.class);
            startActivity(intent);
        } else if (id == R.id.note) {
            Intent intent = new Intent(getApplicationContext(), NoteActivity.class);
            startActivity(intent);
        }else if(id == R.id.now){
            Intent intent = new Intent(getApplicationContext(), NowV2Activity.class);
            startActivity(intent);
        }else if (id == R.id.settings){
            Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
