package com.test.biradam.balessontime;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.test.biradam.balessontime.dao.NoteDao;
import com.test.biradam.balessontime.model.Note;

public class UpdateNoteActivity extends AppCompatActivity {

    EditText editTextNote;
    EditText editTextHeader;

    int id = 0;

    NoteDao noteDao = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_note);

        noteDao = new NoteDao(this.openOrCreateDatabase("BALessonTime", MODE_PRIVATE, null));

        editTextNote = (EditText) findViewById(R.id.editTextNote2);
        editTextHeader = (EditText) findViewById(R.id.editTextHeader2);

        Intent intent = getIntent();
        id = intent.getIntExtra("id",0);

        Note note = noteDao.getNote(id);
        editTextHeader.setText(note.getHeader());
        editTextNote.setText(note.getNote());
    }

    public void save(){
        String noteStr = editTextNote.getText().toString();
        String headerStr = editTextHeader.getText().toString();
        Note note = new Note();
        note.setHeader(headerStr);
        note.setNote(noteStr);
        boolean isUpdated = noteDao.updateNote(note,id);
        if (isUpdated) {
            Toast.makeText(this, "Updated", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(getApplicationContext(),NoteActivity.class);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Error!!!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.update_note_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.save){
            save();
        }else if(item.getItemId() == R.id.delete){
            delete();
        }else if(item.getItemId() == R.id.cancel){
            cancel();
        }
        return super.onOptionsItemSelected(item);
    }

    public void cancel(){
        Intent intent = new Intent(getApplicationContext(),NoteActivity.class);
        startActivity(intent);
    }

    public void delete(){
        AlertDialog.Builder alert = new AlertDialog.Builder(UpdateNoteActivity.this);
        alert.setTitle("Delete");
        alert.setMessage("Are you sure?");
        alert.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                boolean isDeleted = noteDao.deleteNote(id);
                if (isDeleted) {
                    Toast.makeText(UpdateNoteActivity.this, "Deleted", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getApplicationContext(),NoteActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(UpdateNoteActivity.this, "Error!!!", Toast.LENGTH_LONG).show();
                }
            }
        });
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alert.show();
    }

}
