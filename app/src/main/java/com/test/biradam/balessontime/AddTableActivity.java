package com.test.biradam.balessontime;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.test.biradam.balessontime.dao.TableDao;
import com.test.biradam.balessontime.model.Table;

public class AddTableActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_table);
    }

    public void addTable(View view){
        EditText nameET = (EditText) findViewById(R.id.editText);
        String name = nameET.getText().toString();

        TableDao tableDao = new TableDao(this.openOrCreateDatabase("BALessonTime", MODE_PRIVATE, null));
        Table table = new Table();
        table.setName(name);
        table.setPermission(1);

        if(!name.trim().equals("")){
            boolean isAdded = tableDao.addTable(table);
            if (isAdded) {
                Toast.makeText(this, "Added", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(),SettingsActivity.class);
                startActivity(intent);
            } else {
                Toast.makeText(this, "Error!!!", Toast.LENGTH_LONG).show();
            }
        }else{
            Toast.makeText(this, "Please write anything!!!", Toast.LENGTH_LONG).show();
        }
    }

}
