package com.test.biradam.balessontime;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.test.biradam.balessontime.dao.LessonDao;
import com.test.biradam.balessontime.model.Lesson;

public class UpdateLessonActivity extends AppCompatActivity {

    EditText lesson_order;
    EditText lesson_name;
    EditText lesson_type;
    EditText lesson_room;
    EditText teacher_full_name;
    Spinner week_id;
    EditText time_v1;
    EditText time_v2;
    EditText time_v3;
    EditText time_v4;

    int id = 0;

    LessonDao lessonDao = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_lesson);

        lessonDao = new LessonDao(this.openOrCreateDatabase("BALessonTime", MODE_PRIVATE, null));

        lesson_order = (EditText) findViewById(R.id.lesson_order);
        lesson_name = (EditText) findViewById(R.id.lesson_name);
        lesson_type = (EditText) findViewById(R.id.lesson_type);
        lesson_room = (EditText) findViewById(R.id.lesson_room);
        teacher_full_name = (EditText) findViewById(R.id.teacher_full_name);
        week_id = (Spinner) findViewById(R.id.week_id);
        time_v1 = (EditText) findViewById(R.id.lesson_time_v1);
        time_v2 = (EditText) findViewById(R.id.lesson_time_v2);
        time_v3 = (EditText) findViewById(R.id.lesson_time_v3);
        time_v4 = (EditText) findViewById(R.id.lesson_time_v4);

        id = getIntent().getIntExtra("id", 0);
        try {
            Lesson lesson = lessonDao.getLesson(id);
            lesson_order.setText(String.valueOf(lesson.getLesson_order()));
            lesson_name.setText(lesson.getLesson_name());
            lesson_type.setText(lesson.getLesson_type());
            lesson_room.setText(lesson.getLesson_room());
            teacher_full_name.setText(lesson.getTeacher_full_name());
            week_id.setSelection(lesson.getWeekId()-1);
            time_v1.setText(String.format("%02d:%02d", (lesson.getLesson_time()[0] / 3600), (lesson.getLesson_time()[0] % 3600) / 60));
            time_v2.setText(String.format("%02d:%02d", (lesson.getLesson_time()[1] / 3600), (lesson.getLesson_time()[1] % 3600) / 60));
            time_v3.setText(String.format("%02d:%02d", (lesson.getLesson_time()[2] / 3600), (lesson.getLesson_time()[2] % 3600) / 60));
            time_v4.setText(String.format("%02d:%02d", (lesson.getLesson_time()[3] / 3600), (lesson.getLesson_time()[3] % 3600) / 60));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.update_lesson_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.lesson_update){
            update();
        }else if(item.getItemId() == R.id.lesson_cancel){
            Intent intent = new Intent(getApplicationContext(), Main2Activity.class);
            intent.putExtra("id",id);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    public void update(){
        try {
            Lesson lesson = new Lesson();
            lesson.setLesson_order(Integer.valueOf(lesson_order.getText().toString()));
            lesson.setLesson_name(lesson_name.getText().toString());
            lesson.setLesson_type(lesson_type.getText().toString());
            lesson.setLesson_room(lesson_room.getText().toString());
            lesson.setTeacher_full_name(teacher_full_name.getText().toString());
            int[] lesson_times = new int[4];
            lesson_times[0] = Integer.valueOf(time_v1.getText().toString().split(":")[0]) * 60 * 60 + Integer.valueOf(time_v1.getText().toString().split(":")[1]) * 60;
            lesson_times[1] = Integer.valueOf(time_v2.getText().toString().split(":")[0]) * 60 * 60 + Integer.valueOf(time_v2.getText().toString().split(":")[1]) * 60;
            lesson_times[2] = Integer.valueOf(time_v3.getText().toString().split(":")[0]) * 60 * 60 + Integer.valueOf(time_v3.getText().toString().split(":")[1]) * 60;
            lesson_times[3] = Integer.valueOf(time_v4.getText().toString().split(":")[0]) * 60 * 60 + Integer.valueOf(time_v4.getText().toString().split(":")[1]) * 60;
            lesson.setLesson_time(lesson_times);
            lesson.setWeekId(Integer.valueOf(week_id.getSelectedItem().toString()));

            boolean isUpdated = lessonDao.updateLesson(lesson, id);
            if (isUpdated) {
                Toast.makeText(this, "Updated", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(), Main2Activity.class);
                intent.putExtra("id", id);
                startActivity(intent);
            } else {
                Toast.makeText(this, "Error!!!", Toast.LENGTH_LONG).show();
            }
        }catch (Exception ex){
            Toast.makeText(this, "Error!!!", Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }

}
