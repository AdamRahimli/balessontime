package com.test.biradam.balessontime;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.test.biradam.balessontime.dao.LessonDao;
import com.test.biradam.balessontime.model.Lesson;
import com.test.biradam.balessontime.model.TableName;

import java.util.Calendar;
import java.util.List;

public class NowV2Activity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    TextView lessonNameTV;
    TextView lessonTimeTV;
    TextView lessonTypeTV;
    TextView lessonOrderTV;
    TextView teacherTV;
    TextView lessonRoomTV;

    ProgressBar progressBar_45_1;
    ProgressBar progressBar_5;
    ProgressBar progressBar_45_2;

    Runnable run;
    Handler handler;

    int currentTime = 0;

    LessonDao lessonDao = null;

    boolean selected = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_now_v2);

        try {
            SQLiteDatabase myDatabase = this.openOrCreateDatabase("BALessonTime", MODE_PRIVATE, null);
            myDatabase.execSQL("CREATE TABLE IF NOT EXISTS dic (id INTEGER PRIMARY KEY AUTOINCREMENT ,dic_key VARCHAR , dic_value VARCHAR, add_date DATETIME DEFAULT CURRENT_TIMESTAMP)");
            Cursor cursor3 = myDatabase.rawQuery("SELECT * FROM dic WHERE dic_key='table_name'", null);
            cursor3.moveToFirst();
            if (cursor3 != null) {
                TableName.getTableName().setTable_name(cursor3.getString(cursor3.getColumnIndex("dic_value")));
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        lessonDao = new LessonDao(this.openOrCreateDatabase("BALessonTime", MODE_PRIVATE, null));

        deployData();

        lessonNameTV = (TextView) findViewById(R.id.lessonName);
        lessonTimeTV = (TextView) findViewById(R.id.lessonTime);
        lessonTypeTV = (TextView) findViewById(R.id.lessonType);
        lessonOrderTV = (TextView) findViewById(R.id.lessonOrder);
        teacherTV = (TextView) findViewById(R.id.teacher);
        lessonRoomTV = (TextView) findViewById(R.id.lessonRoom);

        progressBar_45_1 = (ProgressBar) findViewById(R.id.progressBar_45_1);
        progressBar_5 = (ProgressBar) findViewById(R.id.progressBar_5);
        progressBar_45_2 = (ProgressBar) findViewById(R.id.progressBar_45_2);

        detectTime();

        if(!TableName.getTableName().getTable_name().equals("")){
            selected = true;
        }
        stopService();
        startService();
    }


    public void startService(){
        Intent serviceIntent = new Intent(this,ServiceNotification.class);
        startService(serviceIntent);
    }

    public void stopService(){
        Intent serviceIntent = new Intent(this,ServiceNotification.class);
        stopService(serviceIntent);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.now_v2, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if(id == R.id.schedule){
            Intent intent = new Intent(getApplicationContext(), ScheduleV3Activity.class);
            startActivity(intent);
        }else if(id == R.id.note){
            Intent intent = new Intent(getApplicationContext(), NoteActivity.class);
            startActivity(intent);
        }else if(id == R.id.now){
            Intent intent = new Intent(getApplicationContext(), NowV2Activity.class);
            startActivity(intent);
        }else if (id == R.id.settings){
            Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void detectTime() {
        handler = new Handler();
        run = new Runnable() {
            @Override
            public void run() {
                Calendar calendar = Calendar.getInstance();

                int hour = calendar.get(Calendar.HOUR_OF_DAY);
                int minute = calendar.get(Calendar.MINUTE);
                int second = calendar.get(Calendar.SECOND);
                currentTime = hour * 3600 + minute * 60 + second;

                int week = calendar.get(Calendar.DAY_OF_WEEK);

                weekDeployText(week, currentTime);

                handler.postDelayed(this, 1000);
            }
        };
        handler.post(run);
    }

    public void deployData() {
        try {
            SQLiteDatabase myDatabase = this.openOrCreateDatabase("BALessonTime", MODE_PRIVATE, null);
            myDatabase.execSQL("CREATE TABLE IF NOT EXISTS TI_45 (id INTEGER PRIMARY KEY AUTOINCREMENT,lesson_order INT(3),lesson_name VARCHAR,lesson_type VARCHAR,lesson_time VARCHAR,lesson_room VARCHAR,teacher_full_name VARCHAR, weekId INT(1))");

            Cursor cursor = myDatabase.rawQuery("SELECT * FROM TI_45", null);
            cursor.moveToFirst();
            if(cursor.getCount() == 0) {
                myDatabase.execSQL("INSERT INTO TI_45 (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId)" +
                        " VALUES(1,'Diskret Riyaziyyat','muhazire','49800,52500,52800,55500','317','Mesinmov Kamil',1)");
                myDatabase.execSQL("INSERT INTO TI_45 (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId) " +
                        " VALUES(2,'Pedaqogika','seminar','56100,58800,59100,61800','409','Ciraqova V.',1)");
                myDatabase.execSQL("INSERT INTO TI_45 (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId) " +
                        " VALUES(1,'Komp modelleshdirme','muhazire','49800,52500,52800,55500','415','Demirov Asif.',2)");
                myDatabase.execSQL("INSERT INTO TI_45 (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId) " +
                        " VALUES(2,'Emeliyyatlar sistemi','muhazire','56100,58800,59100,61800','415','Dadashova Irade.',2)");
                myDatabase.execSQL("INSERT INTO TI_45 (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId) " +
                        " VALUES(3,'Komp modelleshdirme','seminar','62400,65100,65400,68100','411','Demirov Asif',2)");
                myDatabase.execSQL("INSERT INTO TI_45 (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId) " +
                        " VALUES(1,'Pedaqogika','muhazire','49800,52500,52800,55500','310','Mollayeva Elsa',3)");
                myDatabase.execSQL("INSERT INTO TI_45 (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId) " +
                        " VALUES(2,'Riyazi mentiq','muhazire','56100,58800,59100,61800','310','Eliyeva Seadet',3)");
                myDatabase.execSQL("INSERT INTO TI_45 (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId) " +
                        " VALUES(3,'Gendere girish','seminar','62400,65100,65400,68100','415','Cahangirova S.',3)");
                myDatabase.execSQL("INSERT INTO TI_45 (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId) " +
                        " VALUES(1,'Emeliyyatlar sistemi','muhazire','49800,52500,52800,55500','415','Dadashova Irade',4)");
                myDatabase.execSQL("INSERT INTO TI_45 (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId) " +
                        " VALUES(2,'Emeliyyatlar sistemi','seminar','56100,58800,59100,61800','415','Dadashova Irade',4)");
                myDatabase.execSQL("INSERT INTO TI_45 (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId) " +
                        " VALUES(1,'Diskret Riyaziyyat','seminar','49800,52500,52800,55500','310','Eliyeva Seadet',5)");
                myDatabase.execSQL("INSERT INTO TI_45 (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId) " +
                        " VALUES(2,'Riyazi mentiq','seminar','56100,58800,59100,61800','310','Eliyeva Seadet',5)");
                myDatabase.execSQL("INSERT INTO TI_45 (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId) " +
                        " VALUES(3,'Gendere girish','muhazire','62400,65100,65400,68100','415','Cahangirova Aide',5)");

                myDatabase.execSQL("CREATE TABLE IF NOT EXISTS table_name (id INTEGER PRIMARY KEY AUTOINCREMENT ,name VARCHAR , permission INTEGER, add_date DATETIME DEFAULT CURRENT_TIMESTAMP)");
                myDatabase.execSQL("INSERT INTO table_name(name,permission) VALUES ('TI_45',1)");

                myDatabase.execSQL("CREATE TABLE IF NOT EXISTS dic (id INTEGER PRIMARY KEY AUTOINCREMENT ,dic_key VARCHAR , dic_value VARCHAR, add_date DATETIME DEFAULT CURRENT_TIMESTAMP)");
                myDatabase.execSQL("INSERT INTO dic(dic_key,dic_value) VALUES ('table_name','')");
            }
            myDatabase.execSQL("CREATE TABLE IF NOT EXISTS TI_44 (id INTEGER PRIMARY KEY AUTOINCREMENT,lesson_order INT(3),lesson_name VARCHAR,lesson_type VARCHAR,lesson_time VARCHAR,lesson_room VARCHAR,teacher_full_name VARCHAR, weekId INT(1))");

            Cursor cursor2 = myDatabase.rawQuery("SELECT * FROM TI_44", null);
            cursor2.moveToFirst();
            if(cursor2.getCount() == 0) {
                myDatabase.execSQL("INSERT INTO TI_44 (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId)" +
                        " VALUES(1,'Diskret Riyaziyyat','muhazire','49800,52500,52800,55500','317','Mesinmov Kamil',1)");
                myDatabase.execSQL("INSERT INTO TI_44 (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId) " +
                        " VALUES(2,'Riyazi Mentiq','seminar','56100,58800,59100,61800','317','Eliyeva Seadet',1)");
                myDatabase.execSQL("INSERT INTO TI_44 (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId) " +
                        " VALUES(1,'Komp modelleshdirme','muhazire','49800,52500,52800,55500','415','Demirov Asif.',2)");
                myDatabase.execSQL("INSERT INTO TI_44 (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId) " +
                        " VALUES(2,'Emeliyyatlar sistemi','muhazire','56100,58800,59100,61800','415','Dadashova Irade.',2)");
                myDatabase.execSQL("INSERT INTO TI_44 (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId) " +
                        " VALUES(3,'Pedaqogika','seminar','62400,65100,65400,68100','408','Abbaszade J.',2)");
                myDatabase.execSQL("INSERT INTO TI_44 (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId) " +
                        " VALUES(1,'Pedaqogika','muhazire','49800,52500,52800,55500','310','Mollayeva Elsa',3)");
                myDatabase.execSQL("INSERT INTO TI_44 (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId) " +
                        " VALUES(2,'Riyazi mentiq','muhazire','56100,58800,59100,61800','310','Eliyeva Seadet',3)");
                myDatabase.execSQL("INSERT INTO TI_44 (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId) " +
                        " VALUES(3,'Gendere girish','seminar','62400,65100,65400,68100','415','Cahangirova S.',3)");
                myDatabase.execSQL("INSERT INTO TI_44 (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId) " +
                        " VALUES(1,'Emeliyyatlar sistemi','muhazire','49800,52500,52800,55500','415','Dadashova Irade',4)");
                myDatabase.execSQL("INSERT INTO TI_44 (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId) " +
                        " VALUES(2,'Komputer modelleshdirme','seminar','56100,58800,59100,61800','411','Demirov Asef',4)");
                myDatabase.execSQL("INSERT INTO TI_44 (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId) " +
                        " VALUES(1,'Emeliyyatlar sistemi','seminar','49800,52500,52800,55500','311','Dadashova Irade',5)");
                myDatabase.execSQL("INSERT INTO TI_44 (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId) " +
                        " VALUES(2,'Diskret riyaziyyat','seminar','56100,58800,59100,61800','311','Ehmedova Jale',5)");
                myDatabase.execSQL("INSERT INTO TI_44 (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId) " +
                        " VALUES(3,'Gendere girish','muhazire','62400,65100,65400,68100','415','Cahangirova Aide',5)");

                myDatabase.execSQL("CREATE TABLE IF NOT EXISTS table_name (id INTEGER PRIMARY KEY AUTOINCREMENT ,name VARCHAR , permission INTEGER, add_date DATETIME DEFAULT CURRENT_TIMESTAMP)");
                myDatabase.execSQL("INSERT INTO table_name(name,permission) VALUES ('TI_44',1)");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void weekDeployText(int weekId, int currentTime) {
        String lessonName;
        int lessonTime;
        String lessonType;
        int lessonOrder;
        String teacher;
        String lessonRoom;
        try {
            int weekIdFil = (weekId == 1) ? 7 : (weekId - 1) % 7;
            List<Lesson> lessonList = lessonDao.getLessonList(weekIdFil);
            if (lessonList.size() == 0) {
                if(selected) {
                    lessonNameTV.setText("Ders yoxdur");
                }else{
                    lessonNameTV.setText("Settings'den table secin");
                }
                lessonTimeTV.setText("");
                lessonTypeTV.setText("");
                lessonOrderTV.setText("");
                teacherTV.setText("");
                lessonRoomTV.setText("");
                progressBar_45_1.setProgress(0);
                progressBar_5.setProgress(0);
                progressBar_45_2.setProgress(0);
            }
            outerloop:
            for (int i = 0; i < lessonList.size(); i++) {
                int[] lesson_time = lessonList.get(i).getLesson_time();
                for (int j = 0; j < lesson_time.length; j++) {
                    if (currentTime < lesson_time[j]) {
                        lessonName = lessonList.get(i).getLesson_name();
                        lessonTime = lesson_time[j] - currentTime;
                        lessonType = lessonList.get(i).getLesson_type();
                        lessonOrder = lessonList.get(i).getLesson_order();
                        teacher = lessonList.get(i).getTeacher_full_name();
                        lessonRoom = lessonList.get(i).getLesson_room();

                        lessonNameTV.setText("Dersin adi: " + lessonName);
                        String timeFinal = String.format("%02d:%02d:%02d", (lessonTime / 3600), (lessonTime % 3600) / 60,(lessonTime % 3600) % 60);
                        lessonTimeTV.setText(timeFinal);
                        lessonTypeTV.setText("Dersin novu: " + lessonType);
                        lessonOrderTV.setText("Dersin sirasi: " + String.valueOf(lessonOrder));
                        teacherTV.setText("Muellimin adi: " + teacher);
                        lessonRoomTV.setText("Dersin otaqi: " + lessonRoom);
                        if (j == 0) {
                            progressBar_45_1.setProgress(0);
                            progressBar_5.setProgress(0);
                            progressBar_45_2.setProgress(0);
                        } else if (j == 1) {
                            int max = lesson_time[1] - lesson_time[0];
                            progressBar_45_1.setMax(max);
                            progressBar_45_1.setProgress(max - lessonTime);
                            progressBar_5.setProgress(0);
                            progressBar_45_2.setProgress(0);
                        } else if (j == 2) {
                            int max = lesson_time[2] - lesson_time[1];
                            progressBar_45_1.setProgress(currentTime);
                            progressBar_5.setMax(max);
                            progressBar_5.setProgress(max - lessonTime);
                            progressBar_45_2.setProgress(0);
                        } else if (j == 3) {
                            int max = lesson_time[3] - lesson_time[2];
                            progressBar_45_1.setProgress(currentTime);
                            progressBar_5.setProgress(currentTime);
                            progressBar_45_2.setMax(max);
                            progressBar_45_2.setProgress(max - lessonTime);
                        }
                        break outerloop;
                    } else {
                        if(selected) {
                            lessonNameTV.setText("Ders yoxdur");
                        }else{
                            lessonNameTV.setText("Settings'den table secin");
                        }
                        lessonTimeTV.setText("");
                        lessonTypeTV.setText("");
                        lessonOrderTV.setText("");
                        teacherTV.setText("");
                        lessonRoomTV.setText("");
                        progressBar_45_1.setProgress(0);
                        progressBar_5.setProgress(0);
                        progressBar_45_2.setProgress(0);
                    }
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

}
