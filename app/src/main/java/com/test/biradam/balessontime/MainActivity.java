package com.test.biradam.balessontime;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.test.biradam.balessontime.dao.LessonDao;
import com.test.biradam.balessontime.model.Lesson;

import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    TextView lessonNameTV;
    TextView lessonTimeTV;
    TextView lessonTypeTV;
    TextView lessonOrderTV;
    TextView teacherTV;
    TextView lessonRoomTV;

    ProgressBar progressBar_45_1;
    ProgressBar progressBar_5;
    ProgressBar progressBar_45_2;

    Runnable run;
    Handler handler;

    int currentTime = 0;

    LessonDao lessonDao = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lessonDao = new LessonDao(this.openOrCreateDatabase("BALessonTime", MODE_PRIVATE, null));

        deployData();

        lessonNameTV = (TextView) findViewById(R.id.lessonName);
        lessonTimeTV = (TextView) findViewById(R.id.lessonTime);
        lessonTypeTV = (TextView) findViewById(R.id.lessonType);
        lessonOrderTV = (TextView) findViewById(R.id.lessonOrder);
        teacherTV = (TextView) findViewById(R.id.teacher);
        lessonRoomTV = (TextView) findViewById(R.id.lessonRoom);

        progressBar_45_1 = (ProgressBar) findViewById(R.id.progressBar_45_1);
        progressBar_5 = (ProgressBar) findViewById(R.id.progressBar_5);
        progressBar_45_2 = (ProgressBar) findViewById(R.id.progressBar_45_2);

        detectTime();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.schedule) {
            //Intent intent = new Intent(getApplicationContext(), ScheduleActivity.class);
            Intent intent = new Intent(getApplicationContext(), ScheduleV2Activity.class);
            startActivity(intent);
        } else if(item.getItemId() == R.id.note){
            Intent intent = new Intent(getApplicationContext(), NoteActivity.class);
            startActivity(intent);
        } else if(item.getItemId() == R.id.lesson_add){
            Intent intent = new Intent(getApplicationContext(), AddLessonActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    public void detectTime() {
        handler = new Handler();
        run = new Runnable() {
            @Override
            public void run() {
                Calendar calendar = Calendar.getInstance();

                int hour = calendar.get(Calendar.HOUR_OF_DAY);
                int minute = calendar.get(Calendar.MINUTE);
                int second = calendar.get(Calendar.SECOND);
                currentTime = hour * 3600 + minute * 60 + second;

                int week = calendar.get(Calendar.DAY_OF_WEEK);

                weekDeployText(week, currentTime);

                handler.postDelayed(this, 1000);
            }
        };
        handler.post(run);
    }

    public void deployData() {
        try {
            SQLiteDatabase myDatabase = this.openOrCreateDatabase("BALessonTime", MODE_PRIVATE, null);
            myDatabase.execSQL("CREATE TABLE IF NOT EXISTS lesson (id INTEGER PRIMARY KEY AUTOINCREMENT,lesson_order INT(3),lesson_name VARCHAR,lesson_type VARCHAR,lesson_time VARCHAR,lesson_room VARCHAR,teacher_full_name VARCHAR, weekId INT(1))");

            Cursor cursor = myDatabase.rawQuery("SELECT * FROM lesson", null);
            cursor.moveToFirst();
            if(cursor.getCount() == 0) {
                myDatabase.execSQL("INSERT INTO lesson (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId)" +
                        " VALUES(1,'Diskret Riyaziyyat','muhazire','49800,52500,52800,55500','317','Mesinmov Kamil',1)");
                myDatabase.execSQL("INSERT INTO lesson (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId) " +
                        " VALUES(2,'Pedaqogika','seminar','56100,58800,59100,61800','409','Ciraqova V.',1)");
                myDatabase.execSQL("INSERT INTO lesson (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId) " +
                        " VALUES(1,'Komp modelleshdirme','muhazire','49800,52500,52800,55500','415','Demirov Asif.',2)");
                myDatabase.execSQL("INSERT INTO lesson (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId) " +
                        " VALUES(2,'Emeliyyatlar sistemi','muhazire','56100,58800,59100,61800','415','Dadashova Irade.',2)");
                myDatabase.execSQL("INSERT INTO lesson (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId) " +
                        " VALUES(3,'Komp modelleshdirme','seminar','62400,65100,65400,68100','411','Demirov Asif',2)");
                myDatabase.execSQL("INSERT INTO lesson (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId) " +
                        " VALUES(1,'Pedaqogika','muhazire','49800,52500,52800,55500','310','Mollayeva Elsa',3)");
                myDatabase.execSQL("INSERT INTO lesson (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId) " +
                        " VALUES(2,'Riyazi mentiq','muhazire','56100,58800,59100,61800','310','Eliyeva Seadet',3)");
                myDatabase.execSQL("INSERT INTO lesson (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId) " +
                        " VALUES(3,'Gendere girish','seminar','62400,65100,65400,68100','415','Cahangirova S.',3)");
                myDatabase.execSQL("INSERT INTO lesson (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId) " +
                        " VALUES(1,'Emeliyyatlar sistemi','muhazire','49800,52500,52800,55500','415','Dadashova Irade',4)");
                myDatabase.execSQL("INSERT INTO lesson (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId) " +
                        " VALUES(2,'Emeliyyatlar sistemi','seminar','56100,58800,59100,61800','415','Dadashova Irade',4)");
                myDatabase.execSQL("INSERT INTO lesson (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId) " +
                        " VALUES(1,'Diskret Riyaziyyat','seminar','49800,52500,52800,55500','310','Eliyeva Seadet',5)");
                myDatabase.execSQL("INSERT INTO lesson (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId) " +
                        " VALUES(2,'Riyazi mentiq','seminar','56100,58800,59100,61800','310','Eliyeva Seadet',5)");
                myDatabase.execSQL("INSERT INTO lesson (lesson_order,lesson_name ,lesson_type ,lesson_time ,lesson_room ,teacher_full_name, weekId) " +
                        " VALUES(3,'Gendere girish','muhazire','62400,65100,65400,68100','415','Cahangirova Aide',5)");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void weekDeployText(int weekId, int currentTime) {
        String lessonName;
        int lessonTime;
        String lessonType;
        int lessonOrder;
        String teacher;
        String lessonRoom;
        try {
            int weekIdFil = (weekId == 1) ? 7 : (weekId - 1) % 7;
            List<Lesson> lessonList = lessonDao.getLessonList(weekIdFil);
            if (lessonList.size() == 0) {
                lessonNameTV.setText("Ders yoxdur");
                lessonTimeTV.setText("");
                lessonTypeTV.setText("");
                lessonOrderTV.setText("");
                teacherTV.setText("");
                lessonRoomTV.setText("");
                progressBar_45_1.setProgress(0);
                progressBar_5.setProgress(0);
                progressBar_45_2.setProgress(0);
            }
            outerloop:
            for (int i = 0; i < lessonList.size(); i++) {
                int[] lesson_time = lessonList.get(i).getLesson_time();
                for (int j = 0; j < lesson_time.length; j++) {
                    if (currentTime < lesson_time[j]) {
                        lessonName = lessonList.get(i).getLesson_name();
                        lessonTime = lesson_time[j] - currentTime;
                        lessonType = lessonList.get(i).getLesson_type();
                        lessonOrder = lessonList.get(i).getLesson_order();
                        teacher = lessonList.get(i).getTeacher_full_name();
                        lessonRoom = lessonList.get(i).getLesson_room();

                        lessonNameTV.setText("Dersin adi: " + lessonName);
                        String timeFinal = String.format("%02d:%02d:%02d", (lessonTime / 3600), (lessonTime % 3600) / 60,(lessonTime % 3600) % 60);
                        lessonTimeTV.setText(timeFinal);
                        lessonTypeTV.setText("Dersin novu: " + lessonType);
                        lessonOrderTV.setText("Dersin sirasi: " + String.valueOf(lessonOrder));
                        teacherTV.setText("Muellimin adi: " + teacher);
                        lessonRoomTV.setText("Dersin otaqi: " + lessonRoom);
                        if (j == 0) {
                            progressBar_45_1.setProgress(0);
                            progressBar_5.setProgress(0);
                            progressBar_45_2.setProgress(0);
                        } else if (j == 1) {
                            int max = lesson_time[1] - lesson_time[0];
                            progressBar_45_1.setMax(max);
                            progressBar_45_1.setProgress(max - lessonTime);
                            progressBar_5.setProgress(0);
                            progressBar_45_2.setProgress(0);
                        } else if (j == 2) {
                            int max = lesson_time[2] - lesson_time[1];
                            progressBar_45_1.setProgress(currentTime);
                            progressBar_5.setMax(max);
                            progressBar_5.setProgress(max - lessonTime);
                            progressBar_45_2.setProgress(0);
                        } else if (j == 3) {
                            int max = lesson_time[3] - lesson_time[2];
                            progressBar_45_1.setProgress(currentTime);
                            progressBar_5.setProgress(currentTime);
                            progressBar_45_2.setMax(max);
                            progressBar_45_2.setProgress(max - lessonTime);
                        }
                        break outerloop;
                    } else {
                        lessonNameTV.setText("Ders yoxdur");
                        lessonTimeTV.setText("");
                        lessonTypeTV.setText("");
                        lessonOrderTV.setText("");
                        teacherTV.setText("");
                        lessonRoomTV.setText("");
                        progressBar_45_1.setProgress(0);
                        progressBar_5.setProgress(0);
                        progressBar_45_2.setProgress(0);
                    }
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

}
