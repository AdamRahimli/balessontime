package com.test.biradam.balessontime.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.test.biradam.balessontime.model.Lesson;
import com.test.biradam.balessontime.model.TableName;

import java.util.ArrayList;

public class LessonDao {

    private SQLiteDatabase db;

    private String table_name;

    public LessonDao() {
    }

    public LessonDao(SQLiteDatabase db) {
        this.db = db;
        table_name = TableName.getTableName().getTable_name();
    }

    public ArrayList<Lesson> getLessonList(int weekId) throws Exception{
        ArrayList<Lesson> lessonList = new ArrayList<>();
        try {
            SQLiteDatabase myDatabase = db;
            myDatabase.execSQL("CREATE TABLE IF NOT EXISTS "+table_name+" (id INTEGER PRIMARY KEY AUTOINCREMENT,lesson_order INT(3),lesson_name VARCHAR,lesson_type VARCHAR,lesson_time VARCHAR,lesson_room VARCHAR,teacher_full_name VARCHAR, weekId INT(1))");
            Cursor cursor = null;
            if (weekId > 0) {
                cursor = myDatabase.rawQuery("SELECT * FROM "+table_name+" WHERE weekId = " + weekId, null);
            } else {
                cursor = myDatabase.rawQuery("SELECT * FROM "+table_name, null);
            }
            int idIx = cursor.getColumnIndex("id");
            int lesson_orderIx = cursor.getColumnIndex("lesson_order");
            int lesson_nameIx = cursor.getColumnIndex("lesson_name");
            int lesson_typeIx = cursor.getColumnIndex("lesson_type");
            int lesson_timeIx = cursor.getColumnIndex("lesson_time");
            int lesson_roomIx = cursor.getColumnIndex("lesson_room");
            int teacher_full_nameIx = cursor.getColumnIndex("teacher_full_name");
            int weekIdIx = cursor.getColumnIndex("weekId");

            cursor.moveToFirst();

            while (cursor != null) {
                Lesson lesson = new Lesson();
                lesson.setId(cursor.getInt(idIx));
                lesson.setLesson_order(cursor.getInt(lesson_orderIx));
                lesson.setLesson_name(cursor.getString(lesson_nameIx));
                lesson.setLesson_type(cursor.getString(lesson_typeIx));
                int[] lessonTimeFil = new int[4];
                String[] lessonTime = cursor.getString(lesson_timeIx).split(",");
                for (int i = 0; i < lessonTime.length; i++) {
                    lessonTimeFil[i] = Integer.valueOf(lessonTime[i].trim());
                }
                lesson.setLesson_time(lessonTimeFil);
                lesson.setLesson_room(cursor.getString(lesson_roomIx));
                lesson.setTeacher_full_name(cursor.getString(teacher_full_nameIx));
                lesson.setWeekId(cursor.getInt(weekIdIx));
                lessonList.add(lesson);
                cursor.moveToNext();
            }
        } catch (Exception ex) {

        }
        return lessonList;
    }

    public Lesson getLesson(int id) throws Exception {
        Lesson lesson = new Lesson();
        try {
            SQLiteDatabase myDatabase = db;
            myDatabase.execSQL("CREATE TABLE IF NOT EXISTS "+table_name+" (id INTEGER PRIMARY KEY AUTOINCREMENT,lesson_order INT(3),lesson_name VARCHAR,lesson_type VARCHAR,lesson_time VARCHAR,lesson_room VARCHAR,teacher_full_name VARCHAR, weekId INT(1))");
            Cursor cursor = null;
            if (id > 0) {
                cursor = myDatabase.rawQuery("SELECT * FROM "+table_name+" WHERE id = " + id, null);
            } else {
                cursor = myDatabase.rawQuery("SELECT * FROM "+table_name, null);
            }
            int idIx = cursor.getColumnIndex("id");
            int lesson_orderIx = cursor.getColumnIndex("lesson_order");
            int lesson_nameIx = cursor.getColumnIndex("lesson_name");
            int lesson_typeIx = cursor.getColumnIndex("lesson_type");
            int lesson_timeIx = cursor.getColumnIndex("lesson_time");
            int lesson_roomIx = cursor.getColumnIndex("lesson_room");
            int teacher_full_nameIx = cursor.getColumnIndex("teacher_full_name");
            int weekIdIx = cursor.getColumnIndex("weekId");

            cursor.moveToFirst();

            if (cursor != null) {
                lesson.setId(cursor.getInt(idIx));
                lesson.setLesson_order(cursor.getInt(lesson_orderIx));
                lesson.setLesson_name(cursor.getString(lesson_nameIx));
                lesson.setLesson_type(cursor.getString(lesson_typeIx));
                int[] lessonTimeFil = new int[4];
                String[] lessonTime = cursor.getString(lesson_timeIx).split(",");
                for (int i = 0; i < lessonTime.length; i++) {
                    lessonTimeFil[i] = Integer.valueOf(lessonTime[i].trim());
                }
                lesson.setLesson_time(lessonTimeFil);
                lesson.setLesson_room(cursor.getString(lesson_roomIx));
                lesson.setTeacher_full_name(cursor.getString(teacher_full_nameIx));
                lesson.setWeekId(cursor.getInt(weekIdIx));
            }
        } catch (Exception ex) {

        }
        return lesson;
    }

    public boolean addLesson(Lesson lesson){
        boolean result = false;
        try{
            SQLiteDatabase myDatabase = db;
            myDatabase.execSQL("CREATE TABLE IF NOT EXISTS "+table_name+" (id INTEGER PRIMARY KEY AUTOINCREMENT,lesson_order INT(3),lesson_name VARCHAR,lesson_type VARCHAR,lesson_time VARCHAR,lesson_room VARCHAR,teacher_full_name VARCHAR, weekId INT(1))");
            String sql = "INSERT INTO "+table_name+"(lesson_order,lesson_name,lesson_type,lesson_time,lesson_room,teacher_full_name,weekId) VALUES(?,?,?,?,?,?,?)";
            SQLiteStatement statement = db.compileStatement(sql);
            statement.bindLong(1,lesson.getLesson_order());
            statement.bindString(2,lesson.getLesson_name());
            statement.bindString(3,lesson.getLesson_type());
            String timeFil = "";
            for(int i=0;i<lesson.getLesson_time().length;i++){
                if(i == lesson.getLesson_time().length - 1){
                    timeFil += lesson.getLesson_time()[i];
                }else{
                    timeFil += lesson.getLesson_time()[i]+",";
                }
            }
            statement.bindString(4,timeFil);
            statement.bindString(5,lesson.getLesson_room());
            statement.bindString(6,lesson.getTeacher_full_name());
            statement.bindLong(7,lesson.getWeekId());
            statement.execute();
            result = true;
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return result;
    }

    public boolean deleteLesson(int id){
        boolean result = false;
        try {
            SQLiteDatabase myDatabase = db;
            myDatabase.execSQL("CREATE TABLE IF NOT EXISTS "+table_name+" (id INTEGER PRIMARY KEY AUTOINCREMENT,lesson_order INT(3),lesson_name VARCHAR,lesson_type VARCHAR,lesson_time VARCHAR,lesson_room VARCHAR,teacher_full_name VARCHAR, weekId INT(1))");
            String sql = "DELETE FROM "+table_name+" WHERE id = ?";
            SQLiteStatement statement = db.compileStatement(sql);
            statement.bindLong(1, id);
            statement.execute();
            result = true;
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return result;
    }

    public boolean updateLesson(Lesson lesson , int id){
        boolean result = false;
        try{
            SQLiteDatabase myDatabase = db;
            myDatabase.execSQL("CREATE TABLE IF NOT EXISTS "+table_name+" (id INTEGER PRIMARY KEY AUTOINCREMENT,lesson_order INT(3),lesson_name VARCHAR,lesson_type VARCHAR,lesson_time VARCHAR,lesson_room VARCHAR,teacher_full_name VARCHAR, weekId INT(1))");
            String sql = "UPDATE "+table_name+" SET lesson_order=?,lesson_name=?,lesson_type=?,lesson_time=?,lesson_room=?,teacher_full_name=?,weekId=? WHERE id = ?";
            SQLiteStatement statement = db.compileStatement(sql);
            statement.bindLong(1,lesson.getLesson_order());
            statement.bindString(2,lesson.getLesson_name());
            statement.bindString(3,lesson.getLesson_type());
            String timeFil = "";
            for(int i=0;i<lesson.getLesson_time().length;i++){
                if(i == lesson.getLesson_time().length - 1){
                    timeFil += lesson.getLesson_time()[i];
                }else{
                    timeFil += lesson.getLesson_time()[i]+",";
                }
            }
            statement.bindString(4,timeFil);
            statement.bindString(5,lesson.getLesson_room());
            statement.bindString(6,lesson.getTeacher_full_name());
            statement.bindLong(7,lesson.getWeekId());
            statement.bindLong(8,id);
            statement.execute();
            result = true;
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return result;
    }

}
