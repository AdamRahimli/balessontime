package com.test.biradam.balessontime.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.test.biradam.balessontime.model.Note;

import java.util.ArrayList;
import java.util.List;

public class NoteDao {

    private SQLiteDatabase db;

    public NoteDao() {
    }

    public NoteDao(SQLiteDatabase db) {
        this.db = db;
    }

    public List<Note> getNoteList(){
        List<Note> noteList = new ArrayList<>();
        try {
            SQLiteDatabase myDatabase = db;
            myDatabase.execSQL("CREATE TABLE IF NOT EXISTS note (id INTEGER PRIMARY KEY AUTOINCREMENT ,header VARCHAR , note VARCHAR, add_date DATETIME DEFAULT CURRENT_TIMESTAMP)");
            Cursor cursor = myDatabase.rawQuery("SELECT * FROM note", null);
            int idIx = cursor.getColumnIndex("id");
            int headerIx = cursor.getColumnIndex("header");
            int noteIx = cursor.getColumnIndex("note");
            int addDateIx = cursor.getColumnIndex("add_date");
            cursor.moveToFirst();
            while (cursor != null) {
                Note note = new Note();
                note.setId(cursor.getInt(idIx));
                note.setHeader(cursor.getString(headerIx));
                note.setNote(cursor.getString(noteIx));
                note.setAdd_date(cursor.getString(addDateIx));
                noteList.add(note);
                cursor.moveToNext();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return noteList;
    }

    public Note getNote(int id){
        Note note = new Note();
        try {
            SQLiteDatabase myDatabase = db;
            myDatabase.execSQL("CREATE TABLE IF NOT EXISTS note (id INTEGER PRIMARY KEY AUTOINCREMENT ,header VARCHAR , note VARCHAR, add_date DATETIME DEFAULT CURRENT_TIMESTAMP)");
            Cursor cursor = myDatabase.rawQuery("SELECT * FROM note WHERE id = "+id, null);
            int idIx = cursor.getColumnIndex("id");
            int headerIx = cursor.getColumnIndex("header");
            int noteIx = cursor.getColumnIndex("note");
            int addDateIx = cursor.getColumnIndex("add_date");
            cursor.moveToFirst();
            if (cursor != null) {
                note.setId(cursor.getInt(idIx));
                note.setHeader(cursor.getString(headerIx));
                note.setNote(cursor.getString(noteIx));
                note.setAdd_date(cursor.getString(addDateIx));
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return note;
    }

    public boolean addNote(Note note) {
        boolean result = false;
        try {
            SQLiteDatabase myDatabase = db;
            myDatabase.execSQL("CREATE TABLE IF NOT EXISTS note (id INTEGER PRIMARY KEY AUTOINCREMENT ,header VARCHAR , note VARCHAR, add_date DATETIME DEFAULT CURRENT_TIMESTAMP)");
            String sql = "INSERT INTO note(header,note) VALUES (?,?)";
            SQLiteStatement statement = db.compileStatement(sql);
            statement.bindString(1,note.getHeader());
            statement.bindString(2,note.getNote());
            statement.execute();
            result = true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public boolean deleteNote(int id){
        boolean result = false;
        try{
            SQLiteDatabase myDatabase = db;
            myDatabase.execSQL("CREATE TABLE IF NOT EXISTS note (id INTEGER PRIMARY KEY AUTOINCREMENT ,header VARCHAR , note VARCHAR, add_date DATETIME DEFAULT CURRENT_TIMESTAMP)");
            String sql = "DELETE FROM note WHERE id = ?";
            SQLiteStatement statement = db.compileStatement(sql);
            statement.bindLong(1,id);
            statement.execute();
            result = true;
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return result;
    }

    public boolean updateNote(Note note,int id){
        boolean result = false;
        try {
            SQLiteDatabase myDatabase = db;
            myDatabase.execSQL("CREATE TABLE IF NOT EXISTS note (id INTEGER PRIMARY KEY AUTOINCREMENT ,header VARCHAR , note VARCHAR, add_date DATETIME DEFAULT CURRENT_TIMESTAMP)");
            String sql = "UPDATE note SET note = ?, header = ? WHERE id = ?";
            SQLiteStatement statement = db.compileStatement(sql);
            statement.bindString(1,note.getNote());
            statement.bindString(2,note.getHeader());
            statement.bindLong(3,id);
            statement.execute();
            result = true;
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return result;
    }

}
