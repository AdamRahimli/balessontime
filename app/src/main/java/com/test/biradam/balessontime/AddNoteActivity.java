package com.test.biradam.balessontime;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.test.biradam.balessontime.dao.NoteDao;
import com.test.biradam.balessontime.model.Note;

public class AddNoteActivity extends AppCompatActivity {

    NoteDao noteDao = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);

        noteDao = new NoteDao(this.openOrCreateDatabase("BALessonTime", MODE_PRIVATE, null));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.add_note_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.add){
            add();
        }else if(item.getItemId() == R.id.cancel){
            cancel();
        }
        return super.onOptionsItemSelected(item);
    }

    public void cancel(){
        Intent intent = new Intent(getApplicationContext(),NoteActivity.class);
        startActivity(intent);
    }

    public void add() {
        EditText editTextHeader = (EditText) findViewById(R.id.editTextHeader);
        EditText editTextNote = (EditText) findViewById(R.id.editTextNote);
        Note note = new Note();
        note.setHeader(editTextHeader.getText().toString());
        note.setNote(editTextNote.getText().toString());
        if(!editTextHeader.getText().toString().trim().equals("Header") && !editTextNote.getText().toString().trim().equals("Note")) {
            boolean isAdded = noteDao.addNote(note);
            if (isAdded) {
                Toast.makeText(this, "Saved", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(),NoteActivity.class);
                startActivity(intent);
            } else {
                Toast.makeText(this, "Error!!!", Toast.LENGTH_LONG).show();
            }
        }else{
            Toast.makeText(this, "Please write anything!!!", Toast.LENGTH_LONG).show();
        }
    }

}
