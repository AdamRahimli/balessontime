package com.test.biradam.balessontime.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.test.biradam.balessontime.model.Note;
import com.test.biradam.balessontime.model.Table;

import java.util.ArrayList;
import java.util.List;

public class TableDao {

    private SQLiteDatabase db;

    public TableDao() {
    }

    public TableDao(SQLiteDatabase db) {
        this.db = db;
    }

    public List<Table> getTableList(){
        List<Table> tableList = new ArrayList<>();
        try {
            SQLiteDatabase myDatabase = db;
            myDatabase.execSQL("CREATE TABLE IF NOT EXISTS table_name (id INTEGER PRIMARY KEY AUTOINCREMENT ,name VARCHAR , permission INTEGER, add_date DATETIME DEFAULT CURRENT_TIMESTAMP)");
            Cursor cursor = myDatabase.rawQuery("SELECT * FROM table_name", null);
            int idIx = cursor.getColumnIndex("id");
            int nameIx = cursor.getColumnIndex("name");
            int permissionIx = cursor.getColumnIndex("permission");
            int addDateIx = cursor.getColumnIndex("add_date");
            cursor.moveToFirst();
            while (cursor != null) {
                Table table = new Table();
                table.setId(cursor.getInt(idIx));
                table.setName(cursor.getString(nameIx));
                table.setPermission(cursor.getInt(permissionIx));
                table.setAdd_date(cursor.getString(addDateIx));
                tableList.add(table);
                cursor.moveToNext();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return tableList;
    }

    public Table getTable(){
        Table table = new Table();
        try {
            SQLiteDatabase myDatabase = db;
            myDatabase.execSQL("CREATE TABLE IF NOT EXISTS table_name (id INTEGER PRIMARY KEY AUTOINCREMENT ,name VARCHAR , permission INTEGER, add_date DATETIME DEFAULT CURRENT_TIMESTAMP)");
            Cursor cursor = myDatabase.rawQuery("SELECT * FROM table_name", null);
            int idIx = cursor.getColumnIndex("id");
            int nameIx = cursor.getColumnIndex("name");
            int permissionIx = cursor.getColumnIndex("permission");
            int addDateIx = cursor.getColumnIndex("add_date");
            cursor.moveToFirst();
            if (cursor != null) {
                table.setId(cursor.getInt(idIx));
                table.setName(cursor.getString(nameIx));
                table.setPermission(cursor.getInt(permissionIx));
                table.setAdd_date(cursor.getString(addDateIx));
                cursor.moveToNext();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return table;
    }

    public boolean addTable(Table table) {
        boolean result = false;
        try {
            SQLiteDatabase myDatabase = db;
            myDatabase.execSQL("CREATE TABLE IF NOT EXISTS table_name (id INTEGER PRIMARY KEY AUTOINCREMENT ,name VARCHAR , permission INTEGER, add_date DATETIME DEFAULT CURRENT_TIMESTAMP)");
            String sql = "INSERT INTO table_name(name,permission) VALUES (?,?)";
            SQLiteStatement statement = db.compileStatement(sql);
            statement.bindString(1,table.getName());
            statement.bindLong(2,table.getPermission());
            statement.execute();
            result = true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public boolean deleteTable(int id){
        boolean result = false;
        try{
            SQLiteDatabase myDatabase = db;
            myDatabase.execSQL("CREATE TABLE IF NOT EXISTS table_name (id INTEGER PRIMARY KEY AUTOINCREMENT ,name VARCHAR , permission INTEGER, add_date DATETIME DEFAULT CURRENT_TIMESTAMP)");
            String sql = "DELETE FROM table_name WHERE id = ?";
            SQLiteStatement statement = db.compileStatement(sql);
            statement.bindLong(1,id);
            statement.execute();
            result = true;
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return result;
    }

    public boolean updateTable(Table table,int id){
        boolean result = false;
        try {
            SQLiteDatabase myDatabase = db;
            myDatabase.execSQL("CREATE TABLE IF NOT EXISTS table_name (id INTEGER PRIMARY KEY AUTOINCREMENT ,name VARCHAR , permission INTEGER, add_date DATETIME DEFAULT CURRENT_TIMESTAMP)");
            String sql = "UPDATE table_name SET name = ?, permission = ? WHERE id = ?";
            SQLiteStatement statement = db.compileStatement(sql);
            statement.bindString(1,table.getName());
            statement.bindLong(2,table.getPermission());
            statement.bindLong(3,id);
            statement.execute();
            result = true;
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return result;
    }

}
