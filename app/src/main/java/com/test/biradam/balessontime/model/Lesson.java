package com.test.biradam.balessontime.model;

import java.util.Arrays;

public class Lesson {

    private int id;
    private int lesson_order;
    private String lesson_name;
    private String lesson_type;
    private int[] lesson_time;
    private String lesson_room;
    private String teacher_full_name;
    private int weekId;

    public Lesson() {
    }

    public Lesson(String lesson_name) {
        this.lesson_name = lesson_name;
    }

    public int getWeekId() {
        return weekId;
    }

    public void setWeekId(int weekId) {
        this.weekId = weekId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLesson_order() {
        return lesson_order;
    }

    public void setLesson_order(int lesson_order) {
        this.lesson_order = lesson_order;
    }

    public String getLesson_name() {
        return lesson_name;
    }

    public void setLesson_name(String lesson_name) {
        this.lesson_name = lesson_name;
    }

    public String getLesson_type() {
        return lesson_type;
    }

    public void setLesson_type(String lesson_type) {
        this.lesson_type = lesson_type;
    }

    public int[] getLesson_time() {
        return lesson_time;
    }

    public void setLesson_time(int[] lesson_time) {
        this.lesson_time = lesson_time;
    }

    public String getLesson_room() {
        return lesson_room;
    }

    public void setLesson_room(String lesson_room) {
        this.lesson_room = lesson_room;
    }

    public String getTeacher_full_name() {
        return teacher_full_name;
    }

    public void setTeacher_full_name(String teacher_full_name) {
        this.teacher_full_name = teacher_full_name;
    }

    @Override
    public String toString() {
        if(id == 0){
            return lesson_name;
        }else{
            return  "Lesoon: " + lesson_name +
                    "\nTime: " + String.format("%02d:%02d", (lesson_time[0] / 3600), (lesson_time[0] % 3600) / 60) + "-" +
                        String.format("%02d:%02d", (lesson_time[1] / 3600), (lesson_time[1] % 3600) / 60) + " | " +
                        String.format("%02d:%02d", (lesson_time[2] / 3600), (lesson_time[2] % 3600) / 60) + "-" +
                        String.format("%02d:%02d", (lesson_time[3] / 3600), (lesson_time[3] % 3600) / 60) +
                    "\nRoom: " + lesson_room;
        } //String.format("%02d:%02d", (lessonTime / 3600), (lessonTime % 3600) / 60)
    }
}
