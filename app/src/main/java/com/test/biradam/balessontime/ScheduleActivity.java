package com.test.biradam.balessontime;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.test.biradam.balessontime.dao.LessonDao;
import com.test.biradam.balessontime.model.Lesson;

import java.util.ArrayList;

public class ScheduleActivity extends AppCompatActivity {

    ArrayList<Lesson> lessonList = null;

    LessonDao lessonDao = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);

        lessonDao = new LessonDao(this.openOrCreateDatabase("BALessonTime", MODE_PRIVATE, null));

        try {
            lessonList = lessonDao.getLessonList(0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        int old = 0;
        for (int i = 0; i < lessonList.size(); i++) {
            if (old != lessonList.get(i).getWeekId()) {
                old = lessonList.get(i).getWeekId();
                lessonList.add(i, new Lesson("-----" + lessonList.get(i).getWeekId() + "-----"));
            }
        }

        ListView listView = (ListView) findViewById(R.id.listView);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, lessonList);
        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(lessonList.get(position).getId() != 0) {
                    Intent intent = new Intent(getApplicationContext(), Main2Activity.class);
                    intent.putExtra("id", lessonList.get(position).getId());
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.schedule_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.home){
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        }else if(item.getItemId() == R.id.note){
            Intent intent = new Intent(getApplicationContext(),NoteActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

}
