package com.test.biradam.balessontime;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.test.biradam.balessontime.R;
import com.test.biradam.balessontime.dao.LessonDao;
import com.test.biradam.balessontime.model.Lesson;

import java.util.Calendar;

public class Main2Activity extends AppCompatActivity {

    TextView lessonNameTV;
    TextView lessonTimeTV;
    TextView lessonTypeTV;
    TextView lessonOrderTV;
    TextView teacherTV;
    TextView lessonRoomTV;

    ProgressBar progressBar_45_1;
    ProgressBar progressBar_5;
    ProgressBar progressBar_45_2;

    Runnable run;
    Handler handler;

    int id = 0;

    LessonDao lessonDao = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        lessonDao = new LessonDao(this.openOrCreateDatabase("BALessonTime", MODE_PRIVATE, null));

        lessonNameTV = (TextView) findViewById(R.id.lessonName5);
        lessonTimeTV = (TextView) findViewById(R.id.lessonTime5);
        lessonTypeTV = (TextView) findViewById(R.id.lessonType5);
        lessonOrderTV = (TextView) findViewById(R.id.lessonOrder5);
        teacherTV = (TextView) findViewById(R.id.teacher5);
        lessonRoomTV = (TextView) findViewById(R.id.lessonRoom5);

        progressBar_45_1 = (ProgressBar) findViewById(R.id.progressBar_45_15);
        progressBar_5 = (ProgressBar) findViewById(R.id.progressBar_55);
        progressBar_45_2 = (ProgressBar) findViewById(R.id.progressBar_45_25);

        Intent intent = getIntent();
        id = intent.getIntExtra("id", 0);

        detectTime();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main2_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.lesson_update) {
            update();
        } else if (item.getItemId() == R.id.lesson_delete) {
            delete();
        }else if(item.getItemId() == R.id.back){
            Intent intent = new Intent(getApplicationContext(), ScheduleV3Activity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    public void update() {
        Intent intent = new Intent(getApplicationContext(),UpdateLessonActivity.class);
        intent.putExtra("id",id);
        startActivity(intent);
    }

    public void delete() {
        AlertDialog.Builder alert = new AlertDialog.Builder(Main2Activity.this);
        alert.setTitle("Delete");
        alert.setMessage("Are you sure?");
        alert.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                boolean isDeleted = lessonDao.deleteLesson(id);
                if (isDeleted) {
                    Toast.makeText(Main2Activity.this, "Deleted", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getApplicationContext(), ScheduleV3Activity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(Main2Activity.this, "Error!!!", Toast.LENGTH_LONG).show();
                }
            }
        });
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alert.show();
    }

    public void detectTime() {
        handler = new Handler();
        run = new Runnable() {
            @Override
            public void run() {
                Calendar calendar = Calendar.getInstance();

                int hour = calendar.get(Calendar.HOUR_OF_DAY);
                int minute = calendar.get(Calendar.MINUTE);
                int second = calendar.get(Calendar.SECOND);
                int currentTime = hour * 3600 + minute * 60 + second;

                int week = calendar.get(Calendar.DAY_OF_WEEK);
                weekDeployText(currentTime, week);

                handler.postDelayed(this, 1000);
            }
        };
        handler.post(run);
    }


    public void weekDeployText(int currentTime, int week) {
        String lessonName;
        int lessonTime;
        String lessonType;
        int lessonOrder;
        String teacher;
        String lessonRoom;
        int weekId;
        try {
            Lesson lesson = lessonDao.getLesson(id);
            lessonName = lesson.getLesson_name();
            lessonType = lesson.getLesson_type();
            lessonOrder = lesson.getLesson_order();
            teacher = lesson.getTeacher_full_name();
            lessonRoom = lesson.getLesson_room();
            weekId = lesson.getWeekId();
            int weekCurFil = (week == 1) ? 7 : (week - 1) % 7;
            int[] lesson_time = lesson.getLesson_time();
            if (weekId == weekCurFil) {
                for (int j = 0; j < lesson_time.length; j++) {
                    if (currentTime < lesson_time[j]) {
                        lessonTime = lesson_time[j] - currentTime;
                        lessonNameTV.setText("Dersin adi: " + lessonName);
                        String timeFinal = String.format("%02d:%02d:%02d", (lessonTime / 3600), (lessonTime % 3600) / 60, (lessonTime % 3600) % 60);
                        lessonTimeTV.setText(timeFinal);
                        lessonTypeTV.setText("Dersin novu: " + lessonType);
                        lessonOrderTV.setText("Dersin sirasi: " + String.valueOf(lessonOrder));
                        teacherTV.setText("Muellimin adi: " + teacher);
                        lessonRoomTV.setText("Dersin otaqi: " + lessonRoom);
                        if (j == 0) {
                            progressBar_45_1.setProgress(0);
                            progressBar_5.setProgress(0);
                            progressBar_45_2.setProgress(0);
                        } else if (j == 1) {
                            int max = lesson_time[1] - lesson_time[0];
                            progressBar_45_1.setMax(max);
                            progressBar_45_1.setProgress(max - lessonTime);
                            progressBar_5.setProgress(0);
                            progressBar_45_2.setProgress(0);
                        } else if (j == 2) {
                            int max = lesson_time[2] - lesson_time[1];
                            progressBar_45_1.setProgress(currentTime);
                            progressBar_5.setMax(max);
                            progressBar_5.setProgress(max - lessonTime);
                            progressBar_45_2.setProgress(0);
                        } else if (j == 3) {
                            int max = lesson_time[3] - lesson_time[2];
                            progressBar_45_1.setProgress(currentTime);
                            progressBar_5.setProgress(currentTime);
                            progressBar_45_2.setMax(max);
                            progressBar_45_2.setProgress(max - lessonTime);
                        }
                        break;
                    } else {
                        lessonNameTV.setText("Dersin adi: " + lessonName);
                        lessonTypeTV.setText("Dersin novu: " + lessonType);
                        lessonOrderTV.setText("Dersin sirasi: " + String.valueOf(lessonOrder));
                        teacherTV.setText("Muellimin adi: " + teacher);
                        lessonRoomTV.setText("Dersin otaqi: " + lessonRoom);
                        lessonTimeTV.setText("Ders bitib.");
                        progressBar_45_1.setProgress(0);
                        progressBar_5.setProgress(0);
                        progressBar_45_2.setProgress(0);
                    }
                }
            } else if (weekId > weekCurFil) {
                lessonNameTV.setText("Dersin adi: " + lessonName);
                lessonTime = Math.abs(weekId - weekCurFil) * 24 * 3600 + lesson_time[0] - currentTime;
                String timeFinal = String.format("%02d:%02d:%02d", (lessonTime / 3600), (lessonTime % 3600) / 60, (lessonTime % 3600) % 60);
                lessonTimeTV.setText(timeFinal);
                lessonTypeTV.setText("Dersin novu: " + lessonType);
                lessonOrderTV.setText("Dersin sirasi: " + String.valueOf(lessonOrder));
                teacherTV.setText("Muellimin adi: " + teacher);
                lessonRoomTV.setText("Dersin otaqi: " + lessonRoom);
                progressBar_45_1.setProgress(0);
                progressBar_5.setProgress(0);
                progressBar_45_2.setProgress(0);
            } else {
                lessonNameTV.setText("Dersin adi: " + lessonName);
                int sup = 7 - weekCurFil + weekId;
                lessonTime = sup * 24 * 3600 + lesson_time[0] - currentTime;
                String timeFinal = String.format("%02d:%02d:%02d", (lessonTime / 3600), (lessonTime % 3600) / 60, (lessonTime % 3600) % 60);
                lessonTimeTV.setText(timeFinal);
                lessonTypeTV.setText("Dersin novu: " + lessonType);
                lessonOrderTV.setText("Dersin sirasi: " + String.valueOf(lessonOrder));
                teacherTV.setText("Muellimin adi: " + teacher);
                lessonRoomTV.setText("Dersin otaqi: " + lessonRoom);
                progressBar_45_1.setProgress(0);
                progressBar_5.setProgress(0);
                progressBar_45_2.setProgress(0);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
