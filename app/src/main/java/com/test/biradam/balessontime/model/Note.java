package com.test.biradam.balessontime.model;

import java.util.Date;

public class Note {

    private int id;
    private String header;
    private String note;
    private String add_date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getAdd_date() {
        return add_date;
    }

    public void setAdd_date(String add_date) {
        this.add_date = add_date;
    }

    @Override
    public String toString() {
        return  "id:" + id +
                "\nheader: " + header +
                "\n-----note-----\n" + note +
                "\nadd_date:" + add_date;
    }
}
