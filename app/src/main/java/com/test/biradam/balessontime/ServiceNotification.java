package com.test.biradam.balessontime;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;


import com.test.biradam.balessontime.dao.LessonDao;
import com.test.biradam.balessontime.model.Lesson;

import java.util.Calendar;
import java.util.List;

import static com.test.biradam.balessontime.App.CHANNEL_ID;

public class ServiceNotification extends Service {

    int cirTime = 60;

    private String value = "";

    @Override
    public void unbindService(ServiceConnection conn) {
        super.unbindService(conn);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        Intent notificationIntent = new Intent(this, NowV2Activity.class);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

//        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
//                .setContentTitle("Lesson Time")
//                .setContentText(value)
//                .setSmallIcon(R.drawable.ic_android)
//                .setContentIntent(pendingIntent)
//                .build();
//
//        startForeground(1, notification);

        //


        final Handler handler = new Handler();
        Runnable run = new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void run() {
                Calendar calendar = Calendar.getInstance();

                int hour = calendar.get(Calendar.HOUR_OF_DAY);
                int minute = calendar.get(Calendar.MINUTE);
                int second = calendar.get(Calendar.SECOND);
                int currentTime = hour * 3600 + minute * 60 + second;

                int week = calendar.get(Calendar.DAY_OF_WEEK);

                weekDeployText(week, currentTime);

                //System.out.println("Time: " + String.valueOf(cirTime));
                if (Build.VERSION.SDK_INT <= 20) {
                    Intent notificationIntent = new Intent(ServiceNotification.this, NowV2Activity.class);
                    PendingIntent pendingIntent = PendingIntent.getActivity(ServiceNotification.this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                    Notification notification = new NotificationCompat.Builder(ServiceNotification.this, CHANNEL_ID)
                            .setContentTitle("Lesson Time")
                            .setContentText(value)
                            .setSmallIcon(R.drawable.ic_menu_send)
                            .setContentIntent(pendingIntent)
                            .build();
                    startForeground(1, notification);
                } else {
                    Intent notificationIntent = new Intent(ServiceNotification.this, NowV2Activity.class);
                    PendingIntent pendingIntent = PendingIntent.getActivity(ServiceNotification.this, 0, notificationIntent, 0);
                    Notification notification = new NotificationCompat.Builder(ServiceNotification.this, CHANNEL_ID)
                            .setContentTitle("Lesson Time")
                            .setContentText(value)
                            .setSmallIcon(R.drawable.ic_access_time_black_24dp)
                            .setContentIntent(pendingIntent)
                            .build();
                    startForeground(1, notification);
                }
                handler.postDelayed(this, cirTime * 1000);
            }
        };
        handler.post(run);

        //
        return START_STICKY;
    }


    public void weekDeployText(int weekId, int currentTime) {
        String lessonName;
        int lessonTime;
        String lessonRoom;
        try {
            int weekIdFil = (weekId == 1) ? 7 : (weekId - 1) % 7;
            LessonDao lessonDao = new LessonDao(this.openOrCreateDatabase("BALessonTime", MODE_PRIVATE, null));
            List<Lesson> lessonList = lessonDao.getLessonList(weekIdFil);
            if (lessonList.size() == 0) {
                cirTime = (24 * 60 * 60 - currentTime);
                value = "Ders yoxdu";
            }
            outerloop:
            for (int i = 0; i < lessonList.size(); i++) {
                int[] lesson_time = lessonList.get(i).getLesson_time();
                for (int j = 0; j < lesson_time.length; j++) {
                    if (currentTime < lesson_time[j]) {
                        lessonName = lessonList.get(i).getLesson_name();
                        lessonTime = lesson_time[j] - currentTime;
                        lessonRoom = lessonList.get(i).getLesson_room();

                        if (j == 0) {
                            if (lessonTime < 60 * 60) {
                                value = lessonName + " | "
                                        + String.valueOf(lessonRoom) + " | "
                                        + (lessonTime % 3600) / 60 + " minute"
                                        + " | begin";
                                cirTime = 60;
                            } else {
                                if (lessonTime <= 60 * 60 * 2) {
                                    cirTime = lessonTime - 60 * 60;
                                    value = lessonName + " | "
                                            + String.valueOf(lessonRoom) + " | "
                                            + lessonTime / 3600 + " hours"
                                            + " | begin";
                                } else {
                                    value = lessonName + " | "
                                            + String.valueOf(lessonRoom) + " | "
                                            + lessonTime / 3600 + " hours"
                                            + " | begin";
                                    cirTime = (60 * 60);
                                }
                            }
                        } else if (j == 1) {
                            value = lessonName + " | "
                                    + String.valueOf(lessonRoom) + " | "
                                    + (lessonTime % 3600) / 60 + " minute"
                                    + " | 1/2";
                            cirTime = 60;
                        } else if (j == 2) {
                            value = lessonName + " | "
                                    + String.valueOf(lessonRoom) + " | "
                                    + (lessonTime % 3600) / 60 + " minute"
                                    + " | free";
                            cirTime = 60;
                        } else if (j == 3) {
                            value = lessonName + " | "
                                    + String.valueOf(lessonRoom) + " | "
                                    + (lessonTime % 3600) / 60 + " minute"
                                    + " | 2/2";
                            cirTime = 60;
                        }
                        //System.out.println("Time: "+String.valueOf(currentTime));
                        break outerloop;
                    } else {
                        cirTime = (24 * 60 * 60 - currentTime);
                        value = "Ders yoxdu";
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
