package com.test.biradam.balessontime;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;

import com.test.biradam.balessontime.dao.LessonDao;
import com.test.biradam.balessontime.model.Lesson;

public class ScheduleV2Activity extends Activity {

    LessonDao lessonDao = null;

    List<String> ChildList;
    Map<String, List<String>> ParentListItems;
    ExpandableListView expandablelistView;

    // Assign Parent list items here.
    List<String> ParentList = new ArrayList<String>();

    {
        ParentList.add("1");
        ParentList.add("2");
        ParentList.add("3");
        ParentList.add("4");
        ParentList.add("5");
        ParentList.add("6");
        ParentList.add("7");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.schedule_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.home){
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        }else if(item.getItemId() == R.id.note){
            Intent intent = new Intent(getApplicationContext(),NoteActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_bar_schedule_v2);

        lessonDao = new LessonDao(this.openOrCreateDatabase("BALessonTime", MODE_PRIVATE, null));

        try {
            final ArrayList<Lesson> day1 = lessonDao.getLessonList(1);
            final ArrayList<Lesson> day2 = lessonDao.getLessonList(2);
            final ArrayList<Lesson> day3 = lessonDao.getLessonList(3);
            final ArrayList<Lesson> day4 = lessonDao.getLessonList(4);
            final ArrayList<Lesson> day5 = lessonDao.getLessonList(5);
            final ArrayList<Lesson> day6 = lessonDao.getLessonList(6);
            final ArrayList<Lesson> day7 = lessonDao.getLessonList(7);



            ParentListItems = new LinkedHashMap<String, List<String>>();

            for (String HoldItem : ParentList) {
                if (HoldItem.equals("1")) {
                    loadChild(day1);
                } else if (HoldItem.equals("2"))
                    loadChild(day2);
                else if (HoldItem.equals("3"))
                    loadChild(day3);
                else if (HoldItem.equals("4"))
                    loadChild(day4);
                else if (HoldItem.equals("5"))
                    loadChild(day5);
                else if (HoldItem.equals("6"))
                    loadChild(day6);
                else if (HoldItem.equals("7"))
                    loadChild(day7);

                ParentListItems.put(HoldItem, ChildList);
            }

            expandablelistView = (ExpandableListView) findViewById(R.id.expandableListView1);
            final ExpandableListAdapter expListAdapter = new ListAdapter(this, ParentList, ParentListItems);
            expandablelistView.setAdapter(expListAdapter);

            expandablelistView.setOnChildClickListener(new OnChildClickListener() {

                @Override
                public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                    // TODO Auto-generated method stub

                    Intent intent = new Intent(getApplicationContext(), Main2Activity.class);
                    int idSend = -1;
                    switch (groupPosition){
                        case 0:
                            idSend = day1.get(childPosition).getId();
                            intent.putExtra("id", idSend);
                            break;
                        case 1:
                            idSend = day2.get(childPosition).getId();
                            intent.putExtra("id", idSend);
                            break;
                        case 2:
                            idSend = day3.get(childPosition).getId();
                            intent.putExtra("id", idSend);
                            break;
                        case 3:
                            idSend = day4.get(childPosition).getId();
                            intent.putExtra("id", idSend);
                            break;
                        case 4:
                            idSend = day5.get(childPosition).getId();
                            intent.putExtra("id", idSend);
                            break;
                        case 5:
                            idSend = day6.get(childPosition).getId();
                            intent.putExtra("id", idSend);
                            break;
                        case 6:
                            idSend = day7.get(childPosition).getId();
                            intent.putExtra("id", idSend);
                            break;
                    }
                    startActivity(intent);
                    return true;
                }
            });
        } catch (Exception ex) {

        }
    }

    private void loadChild(ArrayList<Lesson> ParentElementsName) {
        ChildList = new ArrayList<String>();
        for (Lesson model : ParentElementsName)
            ChildList.add(model.toString());
    }

}