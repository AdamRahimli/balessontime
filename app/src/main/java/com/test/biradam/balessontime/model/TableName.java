package com.test.biradam.balessontime.model;

import android.database.sqlite.SQLiteDatabase;

public class TableName {

    static TableName tableName;

    private String table_name = "";

    public String getTable_name() {
        return table_name;
    }

    public void setTable_name(String table_name) {
        this.table_name = table_name;
    }

    static public TableName getTableName(){
        if(tableName == null){
            tableName = new TableName();
            return tableName;
        }else{
            return tableName;
        }
    }

}
